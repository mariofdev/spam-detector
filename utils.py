import pickle
import string
import nltk
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords

tfidf = pickle.load(open('./static/vectorizer.pkl', 'rb'))
model = pickle.load(open('./static/model.pkl', 'rb'))

#Preprocess user text
def transform_text(text: str) -> str:
    ps = PorterStemmer()

    text = text.lower()
    text = nltk.word_tokenize(text)

    y = []
    # Remove special characters
    for i in text:
        if i.isalnum():
            y.append(i)
    
    text = y[:]
    y.clear()

    # Remove stop words and punctuation
    for i in text:
        if i not in stopwords.words('english') and i not in string.punctuation:
            y.append(i)
            
    text = y[:]
    y.clear()

    # Steamming
    for i in text:
        y.append(ps.stem(i))
    
            
    return " ".join(y) 

def checkIfSpam(proccesed_text: str) -> str:
    vector_input = tfidf.transform([proccesed_text])

    result = model.predict(vector_input)[0]

    return 'SPAM' if result == 1 else 'HAM'
