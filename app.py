from flask import Flask, request, render_template
from utils import transform_text, checkIfSpam

app = Flask(__name__)

@app.route("/")
def hello_world():
    return render_template('home.html')


@app.post("/classify")
def classify():
    transformed_text = transform_text(request.form['message'])

    result = checkIfSpam(transformed_text)

    return render_template("home.html", result=result)