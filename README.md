# SPAM Detector

## Description
The SPAM Detector is an application powered by a trained AI model(Bernoulli Naive Bayes) that can classify text messages as either spam or not spam (ham). It uses advanced machine learning techniques to analyze and make predictions based on the content of the messages.

The Jupyter Notebook includes detailed information about the dataset employed. With a deep data analysis, data processing, models training,
and model selection.

## Usage
You can use the SPAM Detector to identify whether a text message is spam or not.
## Examples
Here are some examples of messages that can be classified as spam by the SPAM Detector:

1. "As a valued customer, I am pleased to advise you that following recent review of your Mob No. you are awarded with a $1500 Bonus Prize, call 09066364589."

2. "SMS. ac Sptv: The New Jersey Devils and the Detroit Red Wings play Ice Hockey. Correct or Incorrect? End? Reply END SPTV."

Both of these examples are classified as **SPAM** by the SPAM Detector due to their content.

## Getting Started
To get started with the SPAM Detector, follow these steps:

1. Clone this repository to your local machine.
2. Install the required dependencies by running `pip install -r requirements.txt`.
3. Run the application using `flask run`.

